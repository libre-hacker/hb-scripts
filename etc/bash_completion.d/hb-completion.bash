#/usr/bin/env bash

_hb_completions() 
{
    local cur prev opts
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    next="${COMP_WORDS[COMP_WORD+1]}"
    optsHelp="--help --verbose --version"
    opts="activate build deactivate list"

    if [[ ${cur} == -* ]] ; then
        COMPREPLY=( $(compgen -Wc "${optsHelp}" -- ${cur}) )
        return 0
    fi
    if [[ ${prev} ==  hb  ]] ; then
        COMPREPLY=( $(compgen -W "${opts}"  ${cur}) )
        return 0
    fi
    if [[ ${prev} == activate* ]] ; then
        COMPREPLY=( $(compgen  -f -X !${cur}"*.hb" ) )
        return 0
    fi
    if [[ ${prev} == build* ]] ; then
	list=`cat /var/lib/apt/lists/*Packages | grep 'Package: ' | awk '{ print $2}'`
        COMPREPLY=( $(compgen -W "${list}" ${cur} ) )
        return 0
    fi
}

complete -F _hb_completions hb
